local colorscheme = "tokyonight"

vim.o.background = "dark"
vim.g.tokyonight_style = "storm"
vim.g.tokyonight_transparent = true

local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status_ok then
  vim.notify("colorscheme " .. colorscheme .. " not found!")
  return
end


