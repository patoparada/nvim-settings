return {
  settings = {
    latex = {
      build = {
        args = { "-pdf", "-synctex=1", "-interaction=nonstopmode", "-shell-escape", "-file-line-error" },
        executable = "latexkmk",
        onSave = false,
      },
    },
  },
}
