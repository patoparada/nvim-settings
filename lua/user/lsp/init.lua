local status_ok, lspconfig = pcall(require, "lspconfig")
if not status_ok then
  return
end

require("user.lsp.lsp-installer")
require("user.lsp.handlers").setup()

local configs = require("lspconfig/configs")
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

lspconfig.emmet_ls.setup({
  capabilities = capabilities,
  filetypes = {
    "html", "typescriptreact", "javascriptreact", "css", "sass", "scss", "less",
  },
  init_options = {
    html = {
      ["bem.enabled"] = true,
    }
  }
})
