local fn = vim.fn

-- automagically install packer
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing Packer... Close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

-- autocommand that reloads neovim whenever you save 'plugins.lua' file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- using protected call so we don't break in first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

-- popup for packer
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  },
}

-- installing plugins
return packer.startup(function(use)
  -- plugins here
  use "wbthomason/packer.nvim" -- packer manage itself
  use "nvim-lua/popup.nvim" -- vim popup implementation in neovim
  use "nvim-lua/plenary.nvim" -- lots of lua functions used by a lot of plugins

  -- colorscheme
  use "folke/tokyonight.nvim"

  -- cmp things
  use "hrsh7th/nvim-cmp" -- completion plugin itself
  use "hrsh7th/cmp-buffer" -- buffer completion
  use "hrsh7th/cmp-path" -- path completion
  use "hrsh7th/cmp-cmdline" -- cmdline completion
  use "saadparwaiz1/cmp_luasnip" -- snippet completion
  use "hrsh7th/cmp-nvim-lsp"
  use "hrsh7th/cmp-nvim-lua"

  -- snippets
  use "L3MON4D3/LuaSnip" -- snippet engine
  use "rafamadriz/friendly-snippets" -- a bunch of snippets

  -- LSP
  use "neovim/nvim-lspconfig"  --enable LSP
  use "williamboman/nvim-lsp-installer" -- simple to use language server installer

  -- Telescope
  use "nvim-telescope/telescope.nvim"
  use "nvim-telescope/telescope-media-files.nvim"

  -- Treesitter
  use {
    "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
  }
  use "p00f/nvim-ts-rainbow"
  use "nvim-treesitter/playground"

  -- Autopairs
  use "windwp/nvim-autopairs" -- Autopairs, integrates with both cmp and treesitter

  -- Comment
  use "numToStr/Comment.nvim" -- Easily comment stuff
  use 'JoosepAlviste/nvim-ts-context-commentstring'

  -- Git support
  use "lewis6991/gitsigns.nvim"

  -- Nvim-tree
  use "kyazdani42/nvim-tree.lua"
  use "kyazdani42/nvim-web-devicons" -- support for nvim-tree icons

  -- nvim-lastplace
  use "ethanholz/nvim-lastplace"

  -- colorizer, for hex colors
  use "norcalli/nvim-colorizer.lua"

  -- indent marks
  use "lukas-reineke/indent-blankline.nvim"

  -- lualine
  use {
    "nvim-lualine/lualine.nvim",
    requires = { "kyazdani42/nvim-web-devicons", opt = true }
  }

  -- spell checking
  use {
    "lewis6991/spellsitter.nvim",
    config = function ()
      require('spellsitter').setup()
    end
  }

  -- which key plugin
  use { "folke/which-key.nvim" }

  -- automatically set up configuration after cloning packer.nvim
  -- this should be below all plugins
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)
